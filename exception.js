
module.exports = {

    LogicException : class LogicException extends Error {
        constructor(message) { super(message); }
    },

    DatabaseException : class DatabaseException extends Error {
        constructor(message) { super(message); }
    }

}
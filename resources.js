
const exception = require("./exception.js");
const personApi = require("./personApi.js");

module.exports = (app) => {

    app.post("/person", personApi.addPerson);
    app.get("/person", personApi.getPersons);
    app.use(errorHandler);
    
}

function errorHandler(err, req, res, next) {
    console.log(err);
    if(err instanceof exception.LogicException) {
        res.status(400).send(err.message);
    } else if(err instanceof exception.DatabaseException) {
        res.status(500).send("Something went wrong");
    } else {
        res.status(500).send("Server error");
    }
}


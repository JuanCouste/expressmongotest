
var MongoClient = require("mongodb").MongoClient;
var exception = require("./exception.js");

module.exports = call => {
    MongoClient.connect("mongodb://localhost:27017/", (err, db) => {
        if (err) {
            throw new exception.DatabaseException(err.errmsg);
        }
        call(db.db("emtest"));
        db.close();
    });
}
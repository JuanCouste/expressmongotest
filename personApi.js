
var exception = require("./exception.js");
var uuidv4 = require("uuid/v4");
var repository = require("./repository.js");

function addPerson(req, res) {
    let person = req.body;
    if(person.name == undefined) {
        throw new exception.LogicException("Person error");
    }
    let newPerson = {
        id : uuidv4(),
        name : person.name,
        addresses : { }
    }
    if(person.addresses != undefined) {
        person.addresses.forEach(address => {
            addAddress(newPerson, address)
        });
    }
    repository(db => {
        db.collection("person").insertOne(newPerson);
        res.status(200).send(newPerson.id);
    });
}

function addAddress(person, address) {
    if(address.number == undefined) {
        throw new exception.LogicException("Address error");
    }
    addressId++;
    person.addresses[addressId] = {
        number : address.number
    }
    return { id : addressId };
}

function getPersons(req, res) {
    repository(
        db => db.collection("person").find({}).toArray().then(
            data => res.status(200).send(data)
        )
    );
}

module.exports = { addPerson, getPersons }

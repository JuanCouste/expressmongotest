var express = require("express"); 
var bodyParser = require("body-parser");
var resources = require("./resources.js");

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

resources(app);

app.listen(3000, console.log);